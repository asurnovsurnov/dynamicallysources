from repository.getrepository import get_repository_data
from .data_test import list_from_data_exist, list_from_data_not_exist
import pytest


@pytest.mark.asyncio
async def test_get_data_from_sources_exist_table():
    datarepo = await get_repository_data()
    sources_list = {
        "data_1": ["1-10", "31-40"],
        "data_2": ["11-20", "41-50"],
        "data_3": ["21-30", "51-60"]
    }
    result = []
    for table_name in sources_list:
        sour = {table_name: sources_list[table_name]}
        answer = await datarepo.get_data_from_sources(sour)
        result = result + answer
    result = sorted(result, key=lambda x: x['Id'])
    assert result == list_from_data_exist


@pytest.mark.asyncio
async def test_get_data_from_sources_not_exist_table():
    datarepo = await get_repository_data()
    sources_list = {
        "data_4": ["1-10"],
        "data_5": ["11-20", "41-50"]
    }
    result = []
    for table_name in sources_list:
        sour = {table_name: sources_list[table_name]}
        answer = await datarepo.get_data_from_sources(sour)
        result = result + answer
    print(result)

    assert result == list_from_data_not_exist
