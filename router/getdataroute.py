from typing import List
from fastapi import APIRouter, Depends, Query
from repository.getrepository import DataRepository, get_repository_data
import asyncio
import datetime
import logging

logger = logging.getLogger(__name__)

routedata = APIRouter(prefix="/data", tags=['data'])

@routedata.get("/tables")
async def get_data(table_names: List[str] = Query(...), datarepo: DataRepository = Depends(get_repository_data)) -> list:
    """
    Purpose: \n
    Get a sorted list of dictionaries containing the retrieved data.
    """
    try:
        result = []
        logger.info(f"doing: {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {get_data.__name__}")
        for table_range in table_names:
            table_name, id_ = table_range.split(":")
            sour = {table_name: id_.split(",")}
            try:
                answer = await asyncio.wait_for(datarepo.get_data_from_sources(sources_list=sour), timeout=2)
            except asyncio.TimeoutError:
                logger.error(f"error Timeout occurred: {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {get_data.__name__}")
                answer = []
            result = result + answer
        if result:
            sorted_dict_list = sorted(result, key=lambda x: x['Id'])
            return sorted_dict_list
        return result
    except Exception as e:
        logger.error(f"error {e}: {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {get_data.__name__}")
        return []
        #return {"error": str(e)}