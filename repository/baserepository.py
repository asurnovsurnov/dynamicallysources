

class BaseRepository:
    """
    Base repository class for interacting with the database.

    Args:
        async_session: The async session used for database operations.
    """
    def __init__(self, async_session):
        self._async_session = async_session



