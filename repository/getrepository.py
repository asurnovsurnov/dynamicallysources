from .__init__ import BaseRepository, async_session, Data, select
import datetime
import logging

logger = logging.getLogger(__name__)

class DataRepository(BaseRepository):
    """
        Repository class for retrieving data from sources.

        :param: The async session used for database operations.
    """

    async def get_data_from_sources(self, sources_list: dict) -> list:
        """
            Get a list with data from sources

            :param sources_list
            :return: list with data from sources
        """
        session_db = self._async_session()
        async with session_db as session:
            data = []
            table_name = next(iter(sources_list))
            table = Data(name=table_name).get_table(table_name)
            logger.info(f"doing: {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {DataRepository.get_data_from_sources.__name__}")
            for element in sources_list[table_name]:
                between_ids = element.split('-')
                try:
                    query = select(table).filter(table.c.id.between(int(between_ids[0]), int(between_ids[1])))
                    result = await session.execute(query)
                    list_db = result.all()
                except Exception as e:
                    list_db = []
                data = data + list_db
            data = [{'Id': item[0], 'Name': item[1]} for item in data]
            return data


async def get_repository_data():
    """
    Retrieves the data repository.

    Returns:
        An instance of the DataRepository class.
    """
    return DataRepository(async_session)

