from fastapi import FastAPI
import uvicorn
from fastapi.middleware.cors import CORSMiddleware
from router import getdataroute
from fastapi.responses import HTMLResponse
from startpage import pagestart
import datetime
import logging

logger = logging.getLogger(__name__)


app = FastAPI(title='DynamicallySources',
              description='The application is designed to obtain data from several sources in the database. \n'
                          'If an error occurs in one of the sources, an empty list of this source is returned. \n'
                          'When a query to a source lasts more than 2 seconds, an empty list of this table is returned. \n'
                          'The source names and data range are passed in the request. You can choose your own sources to check.')

origins = [
    "http://localhost:3000",
    "http://localhost:5173",
    "http://localhost:8000"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "OPTIONS", "DELETE", "PATCH", "PUT"],
    allow_headers=["Content-Type", "Set-Cookie", "Access-Control-Allow-Headers", "Access-Control-Allow-Origin",
                   "Authorization"],
)

@app.get("/", response_class=HTMLResponse)
async def start():
    """
    Purpose: \n
    Show the user a start page with a link to Swagger to use the API.\n
    """
    logger.info(f"doing: {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {start.__name__}")
    return pagestart.start_generate_html()

app.include_router(getdataroute.routedata)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
