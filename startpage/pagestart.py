def start_generate_html():
    return """
        <html>
            <head>
                <title>Welcome to platform DynamicallySources</title>
                <style>
                    body {
                        background-color: white;
                        font-family: Arial, sans-serif;
                        text-align: center;
                    }

                    h1 {
                        margin-top: 80px;
                    }
                </style>
            </head>
            <body>
                <h3>Welcome to platform DynamicallySources</h3>
                <h5>Test task</h5>
                <p>The application is designed to obtain data from several sources in the database.</p>
                <p>If an error occurs in one of the sources, an empty list of this source is returned.</p> 
                <p>When a query to a source lasts more than 2 seconds, an empty list of this table is returned.</p>
                <p>The source names and data range are passed in the request. You can choose your own sources to check.</p>
                <p>To check the operation of the platform API, follow the link <a href="http://0.0.0.0:8000/docs">here</a>.</p>
                <p>----------------------------------------------------------------------------------------------------</p>
                <p>email: alslight@list.ru.</p>
                <p>telegram: @AlexSurn.</p>
            </body>
        </html>
        """