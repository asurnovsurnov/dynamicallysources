from .appbase import Base
import sqlalchemy as db
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy import Table, Column, Integer, String, MetaData

metadata = MetaData()

class Data(Base):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    @classmethod
    def get_table(cls, tablename):
        return Table(
            tablename,
            metadata,
            Column('id', Integer, primary_key=True),
            Column('name', String),
            extend_existing=True
        )
