from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from config import POSTGRES_DB, POSTGRES_PASSWORD, POSTGRES_HOST, POSTGRES_PORT, POSTGRES_USER
import asyncio

engine = create_async_engine('postgresql+asyncpg://{user}:{passwd}@{host}:{port}/{db}'.format(user=POSTGRES_USER,
                                                                                              passwd=POSTGRES_PASSWORD,
                                                                                              host=POSTGRES_HOST,
                                                                                              port=5432,
                                                                                              db=POSTGRES_DB), pool_size=8)

async_session = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)

Base = declarative_base()