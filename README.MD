# DynamicallySources
The is Test Task.

The application is designed to obtain data from several sources in the database. 
If an error occurs in one of the sources, an empty list of this source is returned. 
When a query to a source lasts more than 2 seconds, an empty list of this table is returned. 
The source names and data range are passed in the request. You can choose your own sources to check.

## Requirements
- Python 3.8 and next.
- PostgreSQL 12.15 and next.

## Install and start

1. Clone the repository to your computer: `git clone https://gitlab.com/asurnovsurnov/dynamicallysources.git`.
2. Create a virtual environment in the *venv* folder with the command:`python3 -m venv venv`.
3. Install from file *requirements.txt* with the command:`pip install -r requirements.txt`.
4. Create file `.env` in the root of the project and fill in the following fields:\
`POSTGRES_USER=<you postgres username>`\
`POSTGRES_PASSWORD=<you postgres password>`\
`POSTGRES_DB=<database name>`\
`POSTGRES_HOST=<database host>`\
`POSTGRES_PORT=<database port>`\
5. Create database and create 3 tables with data.\
Example:\
`CREATE TABLE data_1 (id INT PRIMARY KEY, name VARCHAR(255));`\
`CREATE TABLE data_2 (id INT PRIMARY KEY, name VARCHAR(255));`\
`CREATE TABLE data_3 (id INT PRIMARY KEY, name VARCHAR(255));`\
6. Fill in the tables with the data.\
Example:\
`INSERT INTO data_1 (id, name) VALUES (1, 'Test 1'), (2, 'Test 2'),...,(10, 'Test 10');`
`INSERT INTO data_2 (id, name) VALUES (11, 'Test 11'), (12, 'Test 12'),...,(20, 'Test 20');` and ect.\
7. Run the application with the command `python3 main.py`, and go to the link `http://0.0.0.0:8000` and begin work with API.

### Start Pytest
Run test with the command: `python -m pytest tests/utest.py`

### Example working
In a request via the API, you can pass several table names and the range of requested identifiers of these tables.\
Format: `<table_name>:<id>-<id>, <id>-<id>` or `<table_name>:<id>-<id>`\
forexample: `data_1:1-10,31-40`\
Using button "Add string item" for add table_name and range identifiers in Swager.\

1. If tables exist in database from 3 tables return sorted data:
![img_one](example_image/img_one)

2. If tables exist in database from 2 tables return sorted data:
![img_two](example_image/img_two)

3. If tables not exist in database return empty list:
![img_three](example_image/img_three)




