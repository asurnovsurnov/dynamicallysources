import logging.config
from dotenv import dotenv_values
import yaml
import os, sys

info_env = dotenv_values('.env')


POSTGRES_USER = info_env.get('POSTGRES_USER')
POSTGRES_PASSWORD = info_env.get('POSTGRES_PASSWORD')
POSTGRES_DB = info_env.get('POSTGRES_DB')
POSTGRES_HOST = info_env.get('POSTGRES_HOST')
POSTGRES_PORT = info_env.get('POSTGRES_PORT')


CURRENT_FILE_PATH = os.path.abspath(__file__)
BASE_DIR = os.path.dirname(CURRENT_FILE_PATH)
LOGGING_CONF = BASE_DIR + '/logging.yaml'

if os.path.isfile(LOGGING_CONF) and os.access(LOGGING_CONF, os.R_OK):
    _lc_stream = open(LOGGING_CONF, 'r')
    _lc_conf = yaml.load(_lc_stream, Loader=yaml.FullLoader)
    _lc_stream.close()
    logging.config.dictConfig(_lc_conf)
else:
    print(
        "ERROR: logger config file '%s' not eixsts or not readable\n" %
        LOGGING_CONF)
    sys.exit(1)